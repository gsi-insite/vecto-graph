(defsystem vecto-graphs
  :depends-on (vecto dx-pdf)
  :serial t
  :components
  ((:file "package")
   (:file "share")
   (:file "drawing")
   (:file "vecto")
   (:file "pdf")
   (:file "axes")
   (:file "bar-graph")
   (:file "pie-chart")
   (:file "line-chart")))
